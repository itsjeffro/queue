<?php

namespace Queue;

use Aws\Sqs\SqsClient;
use Queue\Interfaces\MessageInterface;
use Queue\Interfaces\QueueInterface;
use Queue\Messages\SqsMessage;
use Queue\Queue;

class SqsQueue extends Queue implements QueueInterface
{
    /**
     * The SQS instance.
     *
     * @var SqsClient
     */
    private $sqs;

    /**
     * @var string
     */
    private $prefix;
    
    /**
     * @var string
     */
    private $queue;

    /**
     * Instantiate SqsQueue.
     *
     * @param SqsClient $sqs
     * @param string $prefix
     * @param string $queue
     */
    public function __construct(SqsClient $sqs, string $prefix, string $queue)
    {
        $this->sqs = $sqs;
        $this->prefix = $prefix;
        $this->queue = $queue;
    }

    /**
     * Pushes a message onto the queue.
     *
     * @param object $message
     * @return void
     */
    public function push($message)
    {
        $this->sqs->sendMessage([
            'QueueUrl' => $this->queueUrl(),
            'MessageBody' => $this->createPayload($message),
        ]);
    }
    
    /**
     * Retrieves a message from the queue.
     *
     * @return MessageInterface|null
     */
    public function pop()
    {
        $response = $this->sqs->receiveMessage([
            'QueueUrl' => $this->queueUrl(),
            'AttributeNames' => [
                'ApproximateReceiveCount',
            ],
        ]);
        
        if (isset($response['Messages'])) {
            return new SqsMessage($this->sqs, $response['Messages'][0], $this->queueUrl());
        }
    }

    /**
     * Gets the full SQS queue url.
     *
     * @param string $queue
     * @return string
     */
    public function queueUrl(string $queue = ''): string
    {
        $queue = empty($queue) ? $this->queue : $queue;

        return rtrim($this->prefix, '/') . '/' . $queue;
    }
}