<?php

namespace Queue;

use Queue\Messages\Message;

class MessageHandler
{
    /**
     * Call message.
     *
     * @param Message $message
     * @return void
     */
    public function call(Message $message)
    {
        $messageBody = json_decode($message->getRawBody(), true);
        $message = unserialize($messageBody['message']);

        $classMethod = new \ReflectionMethod($message, 'handle');
        $handleParameters = [];

        foreach ($classMethod->getParameters() as $parameters) {
            $class = $parameters->getClass();

            $handleParameters[] = new $class->name;
        }

        $classMethod->invokeArgs($message, $handleParameters);
    }
}
