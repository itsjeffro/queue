<?php

namespace Queue;

abstract class Queue
{
    /**
     * @param object $message
     * @return string
     */
    public function createPayload($message)
    {
        return json_encode([
            'name' => $this->getDisplayName($message),
            'message' => serialize(clone $message),
        ]);
    }

    /**
     * @param object $message
     * @return string
     */
    public function getDisplayName($message): string
    {
        return get_class($message);
    }
}