<?php

namespace Queue\Interfaces;

interface QueuedMessageInterface
{
    /**
     * @return mixed
     */
    public function handle();
}
