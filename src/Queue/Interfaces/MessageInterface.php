<?php

namespace Queue\Interfaces;

interface MessageInterface
{
    /**
     * @return void
     */
    public function delete();

    /**
     * @return string
     */
    public function getRawBody(): string;

    /**
     * @return string
     */
    public function getAttempts(): string;
}