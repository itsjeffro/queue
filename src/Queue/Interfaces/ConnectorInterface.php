<?php

namespace Queue\Interfaces;

interface ConnectorInterface
{
    /**
     * @param array $config
     */
    public function connect(array $config);
}