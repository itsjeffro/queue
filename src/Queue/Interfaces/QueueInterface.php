<?php

namespace Queue\Interfaces;

use Queue\Interfaces\MessageInterface;

interface QueueInterface
{
    /**
     * Pushes a message onto the queue.
     *
     * @param object $message
     * @return void
     */
    public function push($message);
    
    /**
     * Retrieves a message from the queue.
     *
     * @return MessageInterface|null
     */
    public function pop();
}