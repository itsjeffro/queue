<?php

namespace Queue\Interfaces;

interface ExceptionHandlerInterface
{
    /**
     * Log the exception.
     *
     * @param mixed $e
     * @return void
     */
    public function handle($e);
}
