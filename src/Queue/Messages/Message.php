<?php

namespace Queue\Messages;

use Queue\MessageHandler;

abstract class Message
{
    /**
     * Process the messaage.
     *
     * @return void
     */
    public function fire()
    {
        (new MessageHandler)->call($this);

        $this->delete();
    }

    /**
     * Deletes the failed message from the queue.
     *
     * @return void
     */
    public function fail()
    {
        $this->delete();
    }
}
