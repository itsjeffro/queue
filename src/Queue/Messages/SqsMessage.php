<?php

namespace Queue\Messages;

use Aws\Sqs\SqsClient;
use Queue\Interfaces\MessageInterface;
use Queue\Messages\Message;

class SqsMessage extends Message implements MessageInterface
{
    /**
     * The SQS instance.
     *
     * @var SqsClient
     */
    private $sqs;

    /**
     * The SQS raw message.
     *
     * @var array
     */
    private $message;
    
    /**
     * The SQS full queue url.
     *
     * @var string
     */
    private $queueUrl;

    /**
     * Instantiate SqsMessage.
     *
     * @param SqsClient $sqs
     * @param array $message
     * @param string $queueUrl
     */
    public function __construct(SqsClient $sqs, array $message, string $queueUrl)
    {
        $this->sqs = $sqs;
        $this->message = $message;
        $this->queueUrl = $queueUrl;
    }
    
    /**
     * Release the message back into the queue.
     *
     * @param int $messageDelay
     * @return void
     */
    public function release(int $messageDelay = 0)
    {
        $this->sqs->changeMessageVisibility([
            'QueueUrl' => $this->queueUrl,
            'ReceiptHandle' => $this->message['ReceiptHandle'],
            'VisibilityTimeout' => $messageDelay,
        ]);
    }
    
    /**
     * Delete the message from the queue.
     *
     * @return void
     */
    public function delete()
    {
        $this->sqs->deleteMessage([
            'QueueUrl' => $this->queueUrl,
            'ReceiptHandle' => $this->message['ReceiptHandle'],
        ]);
    }
    
    /**
     * The raw body string from the SQS message.
     *
     * @return string
     */
    public function getRawBody(): string
    {
        return $this->message['Body'];
    }
    
    /**
     * The number of times the SQS message has been received.
     *
     * @return string
     */
    public function getAttempts(): string
    {
        return $this->message['Attributes']['ApproximateReceiveCount'];
    }
    
    /**
     * The raw message data from the SQS queue.
     *
     * @return array
     */
    public function getSqsMessage(): array
    {
        return $this->message;
    }
}
