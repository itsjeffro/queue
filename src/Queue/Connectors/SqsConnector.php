<?php

namespace Queue\Connectors;

use Aws\Sqs\SqsClient;
use Queue\Interfaces\ConnectorInterface;
use Queue\Interfaces\QueueInterface;
use Queue\SqsQueue;

class SqsConnector implements ConnectorInterface
{
    /**
     * @param array $config
     * @return QueueInterface
     */
    public function connect(array $config)
    {
        return new SqsQueue(
            new SqsClient($config), $config['prefix'], $config['queue']
        );
    }
}