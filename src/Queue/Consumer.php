<?php

namespace Queue;

use Exception;
use Queue\Exceptions\MaxAttemptsExceededException;
use Queue\Interfaces\QueueInterface;
use Queue\Interfaces\MessageInterface;
use Queue\Interfaces\ExceptionHandlerInterface;
use Throwable;

class Consumer
{
    /**
     * @var QueueInterface
     */
    private $connection;

    /**
     * The queue that the consumer will retrieve messages from.
     *
     * @var string
     */
    private $queue;
    
    /**
     * The memory limit the script can use before exiting.
     *
     * @var int
     */
    private $memoryLimit = 128;
    
    /**
     * The number of seconds the script should sleep.
     *
     * @var int
     */
    private $sleep = 3;
    
    /**
     * The number of seconds should be unavailable for when going back into the queue.
     *
     * @var int
     */
    private $messageDelay = 0;
    
    /**
     * The max attempts the message can be processed.
     *
     * @var int
     */
    private $tries = 0;

    /**
     * The exception handler instance.
     *
     * @var ExceptionHandlerInterface
     */
    private $exceptionHandler;

    /**
     * Instantiate Consumer.
     *
     * @param ExceptionHandlerInterface $exceptionHandler
     */
    public function __construct(ExceptionHandlerInterface $exceptionHandler = null)
    {
        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * Listens to the queue (containing messages) in the loop.
     *
     * @param QueueInterface $connection
     * @param string $queue
     * @return void
     */
    public function daemon(QueueInterface $connection, string $queue = '')
    {
        while (true) {
            $message = $this->getNextMessageFromQueue(
                $connection,
                $queue
            );
            
            if ($message) {
                $this->processMessage($message);
            } else {
                $this->sleep($this->sleep);
            }
            
            if ($this->memoryExceeded($this->memoryLimit)) {
                $this->stop(12);
            } elseif (is_null($message)) {
                $this->stop();
            }
        }
    }
    
    /**
     * Retrieve a message from the queue.
     *
     * @param QueueInterface $connection
     * @param string $queue
     * @return MessageInterface|null
     */
    public function getNextMessageFromQueue(QueueInterface $connection, string $queue)
    {
        if ($message = $connection->pop()) {
            return $message;
        }
    }
    
    /**
     * Process the given message.
     *
     * @param MessageInterface $message
     * @return void
     */
    public function processMessage(MessageInterface $message)
    {
        try {
            if ($this->tries > 0 && $message->getAttempts() > $this->tries) {
                throw new MaxAttemptsExceededException('Max tries exceeded for message.');
            }

            $message->fire();
        } catch (Exception | Throwable $e) {
            $this->exceptionHandler($e);

            $this->handleMessageException($message, $e);
        }
    }
    
    /**
     * Determines how we should handle a message due too a thrown exception.
     *
     * @param MessageInterface $message
     * @return void
     */
    public function handleMessageException(MessageInterface $message, $e)
    {
        if ($e instanceof MaxAttemptsExceededException) {
            $message->fail();

            return;
        }

        $message->release($this->messageDelay);
    }
    
    /**
     * Sleep script for a given number of seconds.
     *
     * @param int $seconds
     * @return void
     */
    public function sleep(int $seconds)
    {
        sleep($seconds);
    }
    
    /**
     * Determine if the memory limit has been exceeded from the script.
     *
     * @param int $memoryLimit
     * @return bool
     */
    public function memoryExceeded(int $memoryLimit): bool
    {
        return (memory_get_usage(true) / 1024 / 1024) >= $memoryLimit;
    }
    
    /**
     * Stop the script.
     *
     * @param int $statusCode
     * @return void
     */
    public function stop(int $statusCode = 0) 
    {
        exit($statusCode);
    }

    /**
     * The handle determines how exceptions should be managed.
     *
     * @param mixed $e
     * @return void
     */
    public function exceptionHandler($e)
    {
        if ($this->exceptionHandler) {
            $this->exceptionHandler->handle($e);
        }
    }
}
