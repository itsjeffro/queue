# Queue

## Connectors

* AWS SQS `Queue\SqsQueue::class`

## Getting started

Configure your SQS connection.

```php
<?php

use Queue\Connectors\SqsConnector;

$connection = (new SqsConnector)
    ->connect([
        'credentials' => [
            'key' => 'IAM-KEY',
            'secret' => 'IAM-SECRET',
        ],
        'region' => 'ap-southeast-2',
        'version' => 'latest',
        'prefix' => 'https://sqs.ap-southeast-2.amazon.com/xxx/',
        'queue' => 'default',
    ]);
```

### Pushing messages to a queue

A message is a normal class which should at least implement `QueueMessageInterface`.

```php
<?php

use Queue\Interfaces\QueuedMessageInterface;

class SendEmail implements QueuedMessageInterface
{
    public function handle()
    {
        // Process email logic
    }
}
```

```php
<?php

use Queue\Connectors\SqsConnector;
use SendEmail;

// Connector configuration

$connection->push(new SendEmail);
```

### Consuming messages

```php
<?php

use Queue\Connectors\SqsConnector;
use Queue\Consumer;

// Connector configuration

$consumer = new Consumer;
$consumer->daemon($connection);
```

## Exception Handler

If a processed message throws an exception, the error will be caught to prevent the consumer from stopping. You may pass an exception handler that implements `Queue\Interfaces\ExceptionHandlerInterface` through the consumer's construct to handle how message exceptions are 
reported or logged.

```php
<?php

use ExceptionHandlerInterface;

class ExceptionHandler implements ExceptionHandlerInterface
{
    public function handle($e)
    {
        // Handle exceptions with a logger such as Monolog
    }
}
```

```php
use ExceptionHandler;

$consumer = new Consumer(new ExceptionHandler);
```

## Supervisord configuration

### Installing Supervisor

```
sudo apt-get install supervisor
```

### Configuring Supervisor

Example supervisord configuration, `consumer.conf`.
 
```
[program:consumer]
process_name=%(program_name)s_%(process_num)02d
command=php /home/user/Consumer.php 
autostart=true
autorestart=true
user=user
numprocs=3 
redirect_stderr=true
stdout_logfile=/home/user/consumer.log
```

### Starting Supervisor

```
sudo supervisorctl reread

sudo supervisorctl update

sudo supervisorctl start consumer:*
```